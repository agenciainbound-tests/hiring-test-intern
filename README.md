# Teste prático - Agência Inbound

## Objetivos
Desenvolver protótipo similar ao exemplo

## Itens obrigatórios
1 - Seguir as melhores práticas de semântica

2 - Manter fontes, cores e espaçamentos fiéis ao modelo apresentado

3 - A página deve ser responsiva com os seguintes break points: 768px e 497px

## Recursos
1 - O projeto está utilizando bootstrap 5.2, utilização opcional
2 - Modelo a ser seguido: https://www.figma.com/file/fhBGuTU2dknZrb53ZqLe5n/Landing-Pages?t=L3mKK8rexADqaMuk-0